##Montagsmaler -- I <3 Mondays


Hallo und vielen Dank für Ihr Interesse an "Montagsmaler"!

Um einen reibungslosen Spielablauf garantieren zu können 
empfehlen wir als Browser mindestens: CHROME Version: 53.0.2785.116 m.

Um am Spielablauf teilnehmen zu können benötigen Sie ein Benutzerprofil, das sie direkt
anlegen können. Ihre Daten werden selbstverständlich vertraulich behandelt und an niemanden weitergegeben.

Da wir eine Server-Client Verbindung nutzen, wird vor Spielstart der Server an der Universität gestartet. Dieser läuft
bereits mit screen im Hintergrund.

Nun können Sie über http://132.199.139.24:8080/ das Spiel starten.

Sollte der Server aus unerfindlichen Gründen nicht laufen, können Sie diesen
über PuTTY durch eine SSH Verbindung über unser Git Repository manuell starten. 
Dies funktioniert wie folgt:

1. cd Montagsmaler
2. cd montagsmaler
3. cd app
4. cd server
5. node server.js

Anschließend sollte die Meldung "Server running on 132.199.139.24:8080" die Verbindung
bestätigen und Sie können loslegen.

Viel Spaß!!! 

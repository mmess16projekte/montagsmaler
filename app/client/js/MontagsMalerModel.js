var MontagsMalerApp = MontagsMalerApp || {};
MontagsMalerApp.Model = function() {
  "use strict";
  /* eslint-env browser  */
  var that = {}, templateString, tmpElement, start, deactivateRoot;
    
  function activateSeite(seite) {
    templateString = document.querySelector(seite).innerHTML;       //template in index.html wird ausgelesen
    tmpElement = document.createElement("div");
    tmpElement.innerHTML = templateString;
    start = document.querySelector(".testTemplateBinding");
    start.appendChild(tmpElement);
  }
    
  function deactivatePrevSeite() {
    deactivateRoot = document.querySelector(".testTemplateBinding");
    deactivateRoot.innerHTML = "";
  }
  
  that.deactivatePrevSeite = deactivatePrevSeite;
  that.activateSeite = activateSeite;
  return that;
};
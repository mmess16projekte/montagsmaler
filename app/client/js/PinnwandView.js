var MontagsMalerApp = MontagsMalerApp || {};
MontagsMalerApp.PinnwandView = function() {
  "use strict";
  /* eslint-env browser  */
    
  var that = {},
    i,
    bilderRef = firebase.database().ref("Bilder"),
    numPics,
    newFrame, newPic, newBackground, newBegriff, newTerm, newURL,
    allBackgrounds, allFrames, allPics, allTerms,
    postedPicsArray,
    bgdiv, framediv, picdiv, termdiv,
    currentPic, currentUser, currentArrayIndex;
    
  function init(value){      
    initAllSelectors();      
    //Filterfunktion durch übergebenen Boolean-Wert
    if(value === true){
      showUserPostedPics();
    } else {          
      showAllPostedPics();
    }
  }
    
  //Initialisiere alle Selektoren an denen später die neu erzeugten Objekte eingefügt werden
  function initAllSelectors() {
    allBackgrounds = document.getElementsByClassName("allBackgrounds");
    allFrames = document.getElementsByClassName("allFrames");
    allPics = document.getElementsByClassName("allPics");
    allTerms = document.getElementsByClassName("allRateBegriffe");
  }
    
  //alle sich in der Datenbank befindenden Bilder werden auf der Pinnwand angezeigt
  function showAllPostedPics() {
    bilderRef.once("value").then(function(snapshot) {
      //Zahl aller in der Database vorhandenen "Bilder"-Pfade
      numPics = snapshot.numChildren();
      //Array aus den Key-Namen der einzelnen "Bilder"
      postedPicsArray = Object.getOwnPropertyNames(snapshot.val());
      
      for(i = 0; i<numPics; i++) {
        
        //erstelle Hintergrund:
        bgdiv = document.createElement("div");                
        bgdiv.className = "background";
        newBackground = document.createElement("img");
        newBackground.src = "res/images/background.bmp";
        bgdiv.appendChild(newBackground); 
        allBackgrounds[0].appendChild(bgdiv);
        
        //erstelle Rahmen:
        framediv = document.createElement("div");                
        framediv.className = "frame";
        newFrame = document.createElement("img");
        newFrame.src = "res/images/bilderrahmen.png";
        framediv.appendChild(newFrame);
        allFrames[0].appendChild(framediv);
        
        //erstelle Bild (Abruf über die DownloadURL in der Firebase-Database vom Firebase-Storage):                
        picdiv = document.createElement("div");                
        picdiv.className = "pic";
        currentPic = postedPicsArray[i];
        newURL = snapshot.child(currentPic+ "/url").val();
        newPic = document.createElement("img");
        newPic.src = newURL;
        picdiv.appendChild(newPic);
        allPics[0].appendChild(picdiv);
        
        //erstelle Begriff (Abruf aus der Firebase-Database):
        termdiv = document.createElement("div");               
        termdiv.className = "rateBegriff";
        newBegriff = snapshot.child(currentPic + "/Begriff").val();
        newTerm = document.createElement("span");
        newTerm.innerHTML = newBegriff;
        termdiv.appendChild(newTerm);
        allTerms[0].appendChild(termdiv);
      }
    });
  }
    
  function showUserPostedPics() {
    bilderRef.once("value").then(function(snapshot) {
      //Zahl aller in der Database vorhandenen "Bilder"-Pfade
      numPics = snapshot.numChildren();
      //Array aus den Key-Namen der einzelnen "Bilder"
      postedPicsArray = Object.getOwnPropertyNames(snapshot.val());
            
      for(i = 0; i<numPics; i++) {
        currentUser = localStorage.loggedUser;
        currentArrayIndex = postedPicsArray[i];
        
        //nur wenn das Array an der aktuellen Stelle den Nutzernamen enthält, wird das Bild auf der Pinnwand angezeigt
        if(currentArrayIndex.includes(currentUser) === true){
            
          //erstelle Hintergrund:    
          bgdiv = document.createElement("div");                
          bgdiv.className = "background";
          newBackground = document.createElement("img");
          newBackground.src = "res/images/background.bmp";
          bgdiv.appendChild(newBackground);
          allBackgrounds[0].appendChild(bgdiv);
            
          //erstelle Rahmen:
          framediv = document.createElement("div");                
          framediv.className = "frame";
          newFrame = document.createElement("img");
          newFrame.src = "res/images/bilderrahmen.png";
          framediv.appendChild(newFrame);
          allFrames[0].appendChild(framediv);
                
          //erstelle Bild (Abruf über die DownloadURL in der Firebase-Database vom Firebase-Storage):
          picdiv = document.createElement("div");
          picdiv.className = "pic";
          currentPic = postedPicsArray[i];
          newURL = snapshot.child(currentPic+ "/url").val();
          newPic = document.createElement("img");
          newPic.src = newURL;
          picdiv.appendChild(newPic);
          allPics[0].appendChild(picdiv);
                    
          //erstelle Begriff (Abruf aus der Firebase-Database):
          termdiv = document.createElement("div");
          termdiv.className = "rateBegriff";
          newBegriff = snapshot.child(currentPic+ "/Begriff").val();
          newTerm = document.createElement("span");
          newTerm.innerHTML = newBegriff;
          termdiv.appendChild(newTerm);
          allTerms[0].appendChild(termdiv);
        }
      }       
    });
  }    
    
  that.init = init;
  return that;
};
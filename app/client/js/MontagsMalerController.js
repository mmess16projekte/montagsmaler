var MontagsMalerApp = MontagsMalerApp || {};
MontagsMalerApp.Controller = function() {
  "use strict";
  /* eslint-env browser */
  var that = {}, cancelParent, problemReport;
  
  /*
  *===================================================================================================== 
  * Hover-Effekte: 
  *=====================================================================================================
  */
  function registerOnPinHoverListener(pin) {
    pin.addEventListener("mouseover", onPinHoverOver);
    pin.addEventListener("mouseout", onPinHoverOut);
  }
  function onPinHoverOver() {
    this.classList.remove("onHoverOutPin");
    this.classList.add("onHoverOverPin");
  }
  function onPinHoverOut() {
    this.classList.remove("onHoverOverPin");
    this.classList.add("onHoverOutPin");
  }
  /*
  *===================================================================================================== 
  * Login: 
  *=====================================================================================================
  */
  function registerSignInListener(signin) {
    signin.addEventListener("click", openPopUpSignIn);
  }
  function openPopUpSignIn() {
    MontagsMalerApp.openPopUp("#signInPopUp");
  }
  /*
  *===================================================================================================== 
  * Signup:
  *=====================================================================================================
  */
  function registerSignUpListener(signup) {
    signup.addEventListener("click", openPopUpSignUp);
  }
  function openPopUpSignUp() {
    MontagsMalerApp.openPopUp("#signUpPopUp");
  }
  /*
  *===================================================================================================== 
  * Passwort ändern:
  *=====================================================================================================
  */
  function registerChangePasswordListener(pwButton) {
    pwButton.addEventListener("click", onChangeDropdownClicked);
  }
    
  function onChangeDropdownClicked() {
    MontagsMalerApp.openPopUp("#passwordPopUp");
  }
  /*
  *=====================================================================================================
  * Nutzerbild ändern:
  *=====================================================================================================
  */
  function registerChangeUserPicListener(userPicButton, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8) {
    pic1.addEventListener("click", changeUserPic);
    pic2.addEventListener("click", changeUserPic);
    pic3.addEventListener("click", changeUserPic);
    pic4.addEventListener("click", changeUserPic);
    pic5.addEventListener("click", changeUserPic);
    pic6.addEventListener("click", changeUserPic);
    pic7.addEventListener("click", changeUserPic);
    pic8.addEventListener("click", changeUserPic);
    userPicButton.addEventListener("click", onChangeUserPicDropdownClicked);
  }
  
  function onChangeUserPicDropdownClicked() {
    MontagsMalerApp.openPopUp("#userPicPopUp");
  }
  /*
  *=====================================================================================================
  * Logout: 
  *=====================================================================================================
  */
  function registerLogOutListener(logout) {
    logout.addEventListener("click", setUserLoggedOut);
  }
  function setUserLoggedOut() {
    localStorage.loggedUser = "";
    location.reload();
  }
  /*
  *===================================================================================================== 
  * Verschiedene Popup-Buttons: 
  *=====================================================================================================
  */
  function registerPopupActions(loginButton, signupButton, changePwButton, cancelButtonLogin, cancelButtonSignup, cancelButtonChangePw, loginPlayButton, cancelButtonLoginPlay, cancelButtonChangeUserPic, ideaProposalSendButton, cancelButtonIdeaProposal) {
    //andere Aktionen:
    loginButton.addEventListener("click", onLoginButtonClicked);
    signupButton.addEventListener("click", onSignupButtonClicked);
    loginPlayButton.addEventListener("click", onLoginPlayButtonClicked);
    changePwButton.addEventListener("click", onChangePwButtonClicked);
    ideaProposalSendButton.addEventListener("click", onSendIdeaProposalClicked);
    
    //Abbruch-Aktionen:
    cancelButtonLogin.addEventListener("click", onCancelButtonClicked);
    cancelButtonSignup.addEventListener("click", onCancelButtonClicked);
    cancelButtonChangePw.addEventListener("click", onCancelButtonClicked);
    cancelButtonLoginPlay.addEventListener("click", onCancelButtonClicked);
    cancelButtonChangeUserPic.addEventListener("click", onCancelButtonClicked);
    cancelButtonIdeaProposal.addEventListener("click", onCancelButtonClicked);
  }
  //Login-Aktion (im Popup):
  //-------------------------------------
  function onLoginButtonClicked() {
    problemReport = this.parentElement.parentElement.children[1];
    MontagsMalerApp.onLoginButtonClicked(problemReport);
  }
  function onLoginPlayButtonClicked() {
    problemReport = this.parentElement.parentElement.children[1];
    MontagsMalerApp.onLoginPlayButtonClicked(problemReport);
  }
  //Signup-Aktion (im Popup):
  //-------------------------------------
  function onSignupButtonClicked() {
    problemReport = this.parentElement.parentElement.children[1];
    MontagsMalerApp.onSignupButtonClicked(problemReport);
  }
  //Passwortänderungs-Aktion (im Popup):
  //-------------------------------------
  function onChangePwButtonClicked() {
    problemReport = this.parentElement.parentElement.children[1];
    MontagsMalerApp.onChangePwButtonClicked(problemReport);
  }
  //Nutzerbildänderungs-Aktion (im Popup):
  //-------------------------------------
  function changeUserPic(event) {
    document.querySelector("#userPic img").src = event.target.src;
    localStorage.selectedUserPic = event.target.src;
    
    MontagsMalerApp.closePopUp("#userPicPopUp");
  }
  //Abbruch-Aktion (im Popup):
  //-------------------------------------
  function onCancelButtonClicked() {
    cancelParent = this.parentNode.parentNode.parentNode.parentNode;
    MontagsMalerApp.closePopUp("#" + cancelParent.id);
  }
  //Abbruch-Aktion (im Popup):
  //------------------------------------- 
  function onSendIdeaProposalClicked() {
    problemReport = this.parentElement.parentElement.children[1];
    MontagsMalerApp.onSendIdeaProposalClicked(problemReport);
  }
  /*
  *=====================================================================================================
  * Bergiff-Vorschlag: 
  *=====================================================================================================
  */
  function registerBegriffVorschlagenListener(begriffVorschlagenBtn) {
    begriffVorschlagenBtn.addEventListener("click", openPopUpIdeaProposal);
  }
  function openPopUpIdeaProposal() {
    MontagsMalerApp.openPopUp("#vorschlagPopup");
  }
  /*
  *=====================================================================================================
  * Play!!!
  *=====================================================================================================
  */
  function registerPlayListener(pin) {
    pin.addEventListener("click", onPlayPinClicked);
  }
    
  function onPlayPinClicked() {
    //activate game-mode:
    if(localStorage.loggedUser !== (null||"" /*||undefined*/)) {
      MontagsMalerApp.onPlayButtonClick();
    } else {
      MontagsMalerApp.openPopUp("#playIfLoggedPopUp");
    }    
  }
  /*
  *===================================================================================================== 
  * Pinnwand: 
  *=====================================================================================================
  */
  function registerPinnwandListener(pin) {
    pin.addEventListener("click", onPinnwandPinClicked);
  }
    
  function onPinnwandPinClicked(event) {
    if(event.target.className === "") {
      MontagsMalerApp.onPinnwandButtonClick(false);
    } else if(event.target.className.includes("status")) {
      MontagsMalerApp.onPinnwandButtonClick(true);
    }   
  }
  /*
  *=====================================================================================================
  * entfernen && wieder hinzufügen Klick-Listener (Popup ist aktiviert): 
  *=====================================================================================================
  */
  function removeClicklistener(play, pinboard, ownPinboard, ideaProposal, signIn, signUp, logOut, changePassword, changeUserPicture) {
    play.removeEventListener("mouseover", onPinHoverOver);
    play.classList.remove("limelight");
    pinboard.removeEventListener("mouseover", onPinHoverOver);
    ownPinboard.removeEventListener("mouseover", onPinHoverOver);
    ideaProposal.removeEventListener("mouseover", onPinHoverOver);
    ideaProposal.classList.remove("shake");
      
    signIn.removeEventListener("click", openPopUpSignIn);
    signUp.removeEventListener("click", openPopUpSignUp);
    ideaProposal.removeEventListener("click", onSendIdeaProposalClicked);
    play.removeEventListener("click", onPlayPinClicked);
    pinboard.removeEventListener("click", onPinnwandPinClicked);
    ownPinboard.removeEventListener("click", onPinnwandPinClicked);
    logOut.removeEventListener("click", setUserLoggedOut);
    changePassword.removeEventListener("click", onChangeDropdownClicked);
    changeUserPicture.removeEventListener("click", onChangeUserPicDropdownClicked);
  }  
  function addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signIn, signUp, logOut, changePassword, changeUserPicture) {
    play.addEventListener("mouseover", onPinHoverOver);
    play.classList.add("limelight");
    pinboard.addEventListener("mouseover", onPinHoverOver);
    ownPinboard.addEventListener("mouseover", onPinHoverOver);
    ideaProposal.addEventListener("mouseover", onPinHoverOver);
    ideaProposal.classList.add("shake");
      
    signIn.addEventListener("click", openPopUpSignIn);
    signUp.addEventListener("click", openPopUpSignUp);
    ideaProposal.addEventListener("click", onSendIdeaProposalClicked);
    play.addEventListener("click", onPlayPinClicked);
    pinboard.addEventListener("click", onPinnwandPinClicked);
    ownPinboard.addEventListener("click", onPinnwandPinClicked);
    logOut.addEventListener("click", setUserLoggedOut);
    changePassword.addEventListener("click", onChangeDropdownClicked);
    changeUserPicture.addEventListener("click", onChangeUserPicDropdownClicked);
  }
  
  that.removeClicklistener = removeClicklistener;
  that.addClicklistenerAgain = addClicklistenerAgain;
  that.registerChangePasswordListener = registerChangePasswordListener;
  that.registerChangeUserPicListener = registerChangeUserPicListener;
  that.registerPopupActions = registerPopupActions;
  that.registerBegriffVorschlagenListener = registerBegriffVorschlagenListener;
  that.registerSignUpListener = registerSignUpListener;
  that.registerSignInListener = registerSignInListener;
  that.registerPinnwandListener= registerPinnwandListener;
  that.registerPlayListener = registerPlayListener;
  that.registerOnPinHoverListener = registerOnPinHoverListener;
  that.registerLogOutListener = registerLogOutListener;
  return that;
};
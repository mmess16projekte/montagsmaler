var MontagsMalerApp = (function() {
  "use strict";
  /* eslint-env browser  */
  var that = {}, 
    montagsMalerModel, montagsMalerController, gamemanager, pinnwandView, templateBinding,
    userNamePanel, userPicPanel, dropDownChangeUserPic, dropDownChangePW, buttonChangePw, dropDownLogOut, signin, signup, logout,
    buttonSignUp, buttonSignIn, buttonSignInPlay, 
    buttonCancelSignup, buttonCancelLogin, buttonCancelLoginPlay, buttonCancelChangeUserPic, buttonCancelChangePw,
    buttonSendIdeaProposal, buttonCancelIdeaProposal, problemReport,
    pinboard, ideaProposal, play, ownPinboard, ownScore,
    user, password, oldPassword, newPassword, loggedUser,
    userPic1, userPic2, userPic3, userPic4, userPic5, userPic6, userPic7, userPic8,
    latestPic, randomPicture, randomPictureNum, allPostedPicturesArray,
    numTerms, randomTermIndex, termObject, termArray = [], randomTerm, i, tempArray = [], countTermOccurrence,
    storeTerm, numUser, counter, ownScoring, userScore,
    randomWiseSayingIndex, wiseSayingsArray, randomWiseSaying, 
    
    begriffeRef = firebase.database().ref("Begriffe"),
    userRef = firebase.database().ref("User"),
    weisheitenRef = firebase.database().ref("Lebensweisheit"),
    bilderRef = firebase.database().ref("Bilder");

  function init() {
    //-------------------------------------------------------------
    // Komponenten sind registriert:
    //-------------------------------------------------------------
    initComponents();
    templateBinding = document.querySelector(".testTemplateBinding");
    montagsMalerModel.activateSeite("#startseite");
    //-------------------------------------------------------------
    // Elemente:
    //-------------------------------------------------------------
    initHeaderElements();
    initPopupElements();
    initMainElements();
    //-------------------------------------------------------------
    // Header-Listener:
    //-------------------------------------------------------------
    initSignUpListener();
    initSignInListener();
    handlePopupActions();
    initDropDownListener();
    checkIfUserIsLogged();
    //-------------------------------------------------------------
    // Öffentlicher-Bereich-Listener:
    //-------------------------------------------------------------
    initPinboardListener();
    initBestScore();
    initWiseSayings();
    initIdeaProposalListener();
    //-------------------------------------------------------------
    // Privater-Bereich-Listener:
    //-------------------------------------------------------------
    initOwnPinboardListener();
    initOwnScoreListener();
    initPlayListener();
  }
  /*
  *===============================================================================================
  * Link zu zentralen Komponenten: 
  *===============================================================================================
  */
  function initComponents() {
    montagsMalerModel = new MontagsMalerApp.Model();
    montagsMalerController = new MontagsMalerApp.Controller();
    gamemanager = new MontagsMalerApp.GameManager();
    pinnwandView = new MontagsMalerApp.PinnwandView();
  }
  /*
  *===============================================================================================
  * Elemente: 
  *===============================================================================================
  */
  function initHeaderElements() {
    //Login && Signup
    signup = document.querySelector("#signup");
    signin = document.querySelector("#signin");
    logout = document.querySelector("#logOut");
    //Nutzer-spezifisch
    userNamePanel = document.getElementById("userName");
    userPicPanel = document.getElementById("userPic");
    dropDownChangeUserPic = document.getElementById("changeUserPic"); 
    dropDownChangePW = document.getElementById("changePW");
    dropDownLogOut = document.getElementById("logOut");
  } 
  function initPopupElements() {
    //Popup-Login-Buttons:
    buttonSignIn = document.querySelector("#signInButton");
    buttonCancelLogin = document.querySelector("#cancelButtonLogin");
    buttonSignInPlay = document.querySelector("#signInPlayButton");
    buttonCancelLoginPlay = document.querySelector("#cancelButtonLoginPlay");
    
    //Popup-Signup-Buttons:
    buttonSignUp = document.querySelector("#signUpButton");
    buttonCancelSignup = document.querySelector("#cancelButtonSignup");
    
    //Popup-Begriff-Vorschlag-Buttons:
    buttonSendIdeaProposal = document.querySelector("#userBegriffVorschlagSendBtn");
    buttonCancelIdeaProposal = document.querySelector("#cancelButtonBegriffVorschlag");
    
    //Popup-Passwortänderungs-Buttons:
    buttonChangePw = document.querySelector("#changePwButton");
    buttonCancelChangePw = document.querySelector("#cancelButtonChangePw");
    
    //Popup-Userbildänderungs-Buttons:
    buttonCancelChangeUserPic = document.querySelector("#cancelButtonChangeUserPic");
  }
  function initMainElements() {
    //im öffentlichen Bereich:
    pinboard = document.querySelector("#pinnwand");
    ideaProposal = document.querySelector("#vorschlag");
    //im privaten Bereich:
    play = document.querySelector("#play");
    ownScore = document.getElementById("own_score");
    ownPinboard = document.getElementById("own_pinnwand");
  }
  /*
  *=============================================================================================== 
  * Login && Signup && Popups && Dropdown-Liste: 
  *===============================================================================================
  */
  //Signup:
  //--------------------------------------------------------------
  function initSignUpListener() {
    montagsMalerController.registerOnPinHoverListener(signup);
    montagsMalerController.registerSignUpListener(signup);
  }
    
  function onSignupButtonClicked(problemReport) {
    user = document.querySelector("#nicknameSignUp").value;
    password = document.querySelector("#passwordSignUp").value;
    //speichere Nutzerdaten in Firebase-Database:
    if(user !== "" && password !== "") {
      montagsMalerController.addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
      onSignUpCreateDataInFirebase(user, password, "#signUpPopUp");
    } else if (user === "" && password !== "") {
      problemReport.innerHTML = "Bitte gib deinen gewünschten Benutzernamen ein!";
    } else if (user !== "" && password === "") {
      problemReport.innerHTML = "Bitte gib dein gewünschtes Passwort ein!";
    } else if (user === "" && password === "") {
      problemReport.innerHTML = "Bitte gib deinen gewünschten Benutzernamen und das gewünschte Passwort ein!";
    } else {
      problemReport.innerHTML = "Dein Account konnte nicht angelegt werden. Versuche es bitte später noch einmal!";
    }
  }
  
  function onSignUpCreateDataInFirebase(userName, password, popUp) {
    problemReport = document.querySelector(popUp).children[0].children[1];
    userRef.once("value").then(function(snapshot) {
      if(snapshot.hasChild(userName) === false) {
        userRef.child(userName).set({
          password: password,
          score: 0,
        });
        problemReport.innerHTML = "";
        logUserInLocalStorage(userName);
        closePopUp(popUp);
      } else {
        problemReport.innerHTML = "Ein Account mit dem Username '" + userName + "' existiert bereits. Bitte überlegen Sie sich einen anderen Usernamen!";
      }
    });
  }
  //Login:
  //--------------------------------------------------------------
  function initSignInListener() {
    montagsMalerController.registerOnPinHoverListener(signin);
    montagsMalerController.registerSignInListener(signin);
  }
  
  function onLoginButtonClicked(problemReport) {
    user = document.querySelector("#nicknameLogIn").value;
    password = document.querySelector("#passwordLogIn").value;
    //lade Nutzerdaten von Firebase-Database:
    if(user !== "" && password !== "") {
      montagsMalerController.addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
      onSignInRetrieveDataFromFirebase(user, password, "#signInPopUp");
    } else if (user === "" && password !== "") {
      problemReport.innerHTML = "Bitte gib deinen Benutzernamen ein!";
    } else if (user !== "" && password === "") {
      problemReport.innerHTML = "Bitte gib dein Passwort ein!";
    } else if (user === "" && password === "") {
      problemReport.innerHTML = "Bitte gib deinen Benutzernamen und dein Passwort ein!";
    } else {
      problemReport.innerHTML = "Der Login ist fehlgeschlagen. Versuche es bitte später noch einmal!";
    }
  }
  function onLoginPlayButtonClicked(problemReport) {
    user = document.querySelector("#nicknameLogInPlay").value;
    password = document.querySelector("#passwordLogInPlay").value;
    //lade Nutzerdaten von Firebase-Database:
    if(user !== "" && password !== "") {
      montagsMalerController.addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
      onSignInRetrieveDataFromFirebase(user, password, "#playIfLoggedPopUp");
    } else if (user === "" && password !== "") {
      problemReport.innerHTML = "Bitte gib deinen Benutzernamen ein!";
    } else if (user !== "" && password === "") {
      problemReport.innerHTML = "Bitte gib dein Passwort ein!";
    } else if (user === "" && password === "") {
      problemReport.innerHTML = "Bitte gib deinen Benutzernamen und dein Passwort ein!";
    } else {
      problemReport.innerHTML = "Der Login ist fehlgeschlagen. Versuche es bitte später noch einmal!";
    }
  }
  
  function onSignInRetrieveDataFromFirebase(userName, password, popUp) {
    problemReport = document.querySelector(popUp).children[0].children[1];
    userRef.once("value").then(function(snapshot) {
      if((snapshot.hasChild(userName) === true) && (snapshot.child(userName + "/password").val() === password)) {
        problemReport.innerHTML = "";
        logUserInLocalStorage(userName);
        closePopUp(popUp);
      } else if(snapshot.hasChild(userName) === false) {
        problemReport.innerHTML = "Der Username '" + userName + "' existiert nicht. Bitte lege zuerst einen Account an!";
      } else if((snapshot.hasChild(userName) === true) && (snapshot.child(userName + "/password").val() !== password)) {
        problemReport.innerHTML = "Dein Passwort ist falsch, bitte versuche es später noch einmal.";
      }
    });
  }
  //Popups:
  //--------------------------------------------------------------
  function handlePopupActions() {
    montagsMalerController.registerPopupActions(buttonSignIn, buttonSignUp, buttonChangePw, buttonCancelLogin, buttonCancelSignup, buttonCancelChangePw, buttonSignInPlay, buttonCancelLoginPlay, buttonCancelChangeUserPic, buttonSendIdeaProposal, buttonCancelIdeaProposal);
  }
  function openPopUp(popUpId) {
    document.querySelector(popUpId).classList.remove("hidden");
    document.querySelector(popUpId).classList.add("fadeIn");
    templateBinding.classList.add("lowerOpacity");
    document.querySelector(".problemReport").innerHTML = "";
      
    montagsMalerController.removeClicklistener(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
  }
  function closePopUp(popUpId) {
    templateBinding.classList.remove("lowerOpacity");
    document.querySelector(popUpId).classList.remove("fadeIn");
    document.querySelector(popUpId).classList.add("fadeOut");
    setTimeout(function() {
      document.querySelector(popUpId).classList.remove("fadeOut");
      document.querySelector(popUpId).classList.add("hidden");
    }, 1000);
      
    montagsMalerController.addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
  }
  
  //Dropdown-Liste:
  //--------------------------------------------------------------
  function initDropDownListener() {
    montagsMalerController.registerLogOutListener(dropDownLogOut);
    montagsMalerController.registerChangePasswordListener(dropDownChangePW);
    
    userPic1 = document.getElementById("userPic1");
    userPic2 = document.getElementById("userPic2");
    userPic3 = document.getElementById("userPic3");
    userPic4 = document.getElementById("userPic4");
    userPic5 = document.getElementById("userPic5");
    userPic6 = document.getElementById("userPic6");
    userPic7 = document.getElementById("userPic7");
    userPic8 = document.getElementById("userPic8");
    montagsMalerController.registerChangeUserPicListener(dropDownChangeUserPic, userPic1, userPic2, userPic3, userPic4, userPic5, userPic6, userPic7, userPic8);
  }
    
  function onChangePwButtonClicked(problemReport) {
    oldPassword = document.querySelector("#oldPassword").value;
    newPassword = document.querySelector("#newPassword").value;
    loggedUser = localStorage.loggedUser;
    //scanne && überprüfe Nutzerdaten in Firebase-Database && gebe neues Passwort ein:
    if(oldPassword !== "" && newPassword !== "") {
      problemReport.innerHTML = "";
      document.querySelector("#oldPassword").value = "";
      document.querySelector("#newPassword").value = "";
      montagsMalerController.addClicklistenerAgain(play, pinboard, ownPinboard, ideaProposal, signin, signup, logout, dropDownChangePW, dropDownChangeUserPic);
      onChangePWReadAndWriteDataFromFirebase(loggedUser, oldPassword, newPassword, "#passwordPopUp");
    } else if (oldPassword === "" && newPassword !== "") {
      problemReport.innerHTML = "Bitte gib dein altes Passwort ein!";
    } else if (oldPassword !== "" && newPassword === "") {
      problemReport.innerHTML = "Bitte gib dein neues Passwort ein!";
    } else if (oldPassword === "" && newPassword === "") {
      problemReport.innerHTML = "Bitte gib sowohl dein altes als auch dein neues Passwort ein!";
    } else {
      problemReport.innerHTML = "Das Passwort konnte nicht geändert werden. Versuche es bitte später noch einmal!";
    }
  }
    
  function onChangePWReadAndWriteDataFromFirebase(loggedUser, oldPassword, newPassword, popUp) {
    problemReport = document.querySelector(popUp).children[0].children[1];
    
    userRef.once("value").then(function(snapshot) {
      if((snapshot.hasChild(loggedUser) === true) && (snapshot.child(loggedUser + "/password").val() === oldPassword)) {
        userRef.child(loggedUser + "/password").set(newPassword);
        closePopUp(popUp);
        setTimeout(function() {
          document.querySelector("#oldPassword").value = "";
          document.querySelector("#newPassword").value = "";
        }, 1000);
      } else if((snapshot.hasChild(loggedUser) === true) && (snapshot.child(loggedUser + "/password").val() !== oldPassword)) {
        problemReport.innerHTML = "Ihr altes Passwort ist falsch, bitte versuchen Sie es noch einmal";
        document.querySelector("#oldPassword").value = "";
      }
    });
  }
  /*
  *=============================================================================================== 
  * LocalStorage - Nutzer eingeloggt: 
  *===============================================================================================
  */
  function checkIfUserIsLogged() {
    if((typeof localStorage.loggedUser === "undefined")||(localStorage.loggedUser === "" || null)){
      setUserHidden();
    } else {
      userNamePanel.innerHTML = localStorage.loggedUser;
      document.querySelector("#userPic img").src = localStorage.selectedUserPic;
      setUserVisible();
    }
  }
  function logUserInLocalStorage(username) {
    localStorage.loggedUser = username;
    if((typeof localStorage.selectedUserPic === "undefined") || (localStorage.selectedUserPic === "" || null)){
      localStorage.selectedUserPic = "res/images/profilbild_bee_m.jpg";
    }
    document.querySelector("#userPic img").src = localStorage.selectedUserPic;
    userNamePanel.innerHTML = username;
    setUserVisible();
  }
  function setUserVisible() {      
    signup.className = "statusHidden";
    signin.className = "statusHidden";      
    ownPinboard.className = "status";
    ownScore.className = "status";    
    userNamePanel.className = "user";
    userPicPanel.className = "user";
  }
  function setUserHidden() {      
    signup.className = "status";
    signin.className = "status";      
    ownPinboard.className = "statusHidden";
    ownScore.className = "statusHidden";    
    userNamePanel.className = "userHidden";
    userPicPanel.className = "userHidden";
  } 
  /*
  *===============================================================================================
  * Pinnwand (öffentlicher && privater Bereich):
  *===============================================================================================
  */
  function initPinboardListener() {
    montagsMalerController.registerOnPinHoverListener(pinboard);
    montagsMalerController.registerPinnwandListener(pinboard);
    showLatestPic();
  }
    
  function showLatestPic() {
    bilderRef.once("value").then(function(snapshot) {
      randomPictureNum = getRandomIndex(snapshot.numChildren());
      allPostedPicturesArray = Object.getOwnPropertyNames(snapshot.val());
      randomPicture = allPostedPicturesArray[randomPictureNum];
      latestPic = snapshot.child(randomPicture + "/url").val();
      document.querySelector("#previewPinnwandImg").src = latestPic;
    });
  }
    
  function initOwnPinboardListener() {
    montagsMalerController.registerOnPinHoverListener(ownPinboard); 
    montagsMalerController.registerPinnwandListener(ownPinboard);
  }
    
  function onPinnwandButtonClick(bool) {
    montagsMalerModel.deactivatePrevSeite();
    montagsMalerModel.activateSeite("#pinnwandseite");
    $().ready(new function(){
      pinnwandView.init(bool);
    });
  }
  /*
  *=============================================================================================== 
  * Highscore - Bestenliste:
  *===============================================================================================
  */
  function initBestScore() {
    //zuerst wird LocalStorage aufgerufen (für sanften Firebase-Callback-Übergang):
    document.querySelector("#firstPlace").innerHTML = localStorage.firstPlace;
    document.querySelector("#firstPoints").innerHTML = " [" + localStorage.firstPoints + " Pkt]";
    document.querySelector("#secondPlace").innerHTML = localStorage.secondPlace;
    document.querySelector("#secondPoints").innerHTML = " [" + localStorage.secondPoints + " Pkt]";
    document.querySelector("#thirdPlace").innerHTML = localStorage.thirdPlace; 
    document.querySelector("#thirdPoints").innerHTML = " [" + localStorage.thirdPoints + " Pkt]";
    
    //dann werden Nutzer + Punkte von Firebase-Database abgerufen:
    userRef.once("value").then(function(snapshot) {
      counter = 0;
      numUser = snapshot.numChildren();
      userRef.orderByChild("score").on("child_added", function(childSnapshot) {
        counter = counter + 1;
          
        if (counter === numUser) {
          //speichere in LocalStorage:
          localStorage.firstPlace = childSnapshot.key;
          localStorage.firstPoints = childSnapshot.child("score").val();
          //lade aus Firebase-Database:
          document.querySelector("#firstPlace").innerHTML = childSnapshot.key;
          document.querySelector("#firstPoints").innerHTML = " [" + childSnapshot.child("score").val() + " Pkt]";
        } else if (counter === numUser-1) {
          //speichere in LocalStorage:
          localStorage.secondPlace = childSnapshot.key;
          localStorage.secondPoints = childSnapshot.child("score").val();
          //lade aus Firebase-Database:
          document.querySelector("#secondPlace").innerHTML = childSnapshot.key;
          document.querySelector("#secondPoints").innerHTML = " [" + childSnapshot.child("score").val() + " Pkt]";
        } else if (counter === numUser-2) {
          //speichere in LocalStorage:
          localStorage.thirdPlace = childSnapshot.key;
          localStorage.thirdPoints = childSnapshot.child("score").val();
          //lade aus Firebase-Database:
          document.querySelector("#thirdPlace").innerHTML = childSnapshot.key; 
          document.querySelector("#thirdPoints").innerHTML = " [" + childSnapshot.child("score").val() + " Pkt]";
        }
      });
    });
  }
  /*
  *===============================================================================================
  * Lebensweisheiten:
  *===============================================================================================
  */
  function initWiseSayings() {
    //bevor die Daten aus Firebase-Database geladen werden - fülle div mit Inhalt aus LocalStorage (nicht leer):
    document.querySelector("#weisheitbeispiel").innerHTML = localStorage.weisheit;
      
    //lade Zufälliges aus Firebase-Database:
    weisheitenRef.once("value").then(function(snapshot) {
      randomWiseSayingIndex = getRandomIndex(snapshot.numChildren());
      wiseSayingsArray = Object.getOwnPropertyNames(snapshot.val());
      randomWiseSaying = wiseSayingsArray[randomWiseSayingIndex];       
      //speichere aktuelle Lebensweisheit in LocalStorage:
      localStorage.weisheit = snapshot.child(randomWiseSaying + "/text").val();
      //lade aud Firebase-Database:
      document.querySelector("#weisheitbeispiel").innerHTML = snapshot.child(randomWiseSaying + "/text").val();
      play = document.querySelector("#play");
      setTimeout(function(){ play.innerHTML="PLAY"; play.style.pointerEvents = "auto"; }, 1000);
    });
  }
  /*
  *===============================================================================================
  * Begriff-Vorschlag: 
  *===============================================================================================
  */
  function initIdeaProposalListener() {
      //http://stackoverflow.com/questions/11365632/how-to-detect-when-the-user-presses-enter-in-an-input-field
    document.getElementById("userBegriffVorschlag").onkeypress = function(e) {
      if (!e) e = window.event;
      var keyCode = e.keyCode || e.which;
      if (keyCode == "13"){
        return false;
      }
    };    
    montagsMalerController.registerOnPinHoverListener(ideaProposal);
    montagsMalerController.registerBegriffVorschlagenListener(ideaProposal);
  }
    
  //sende Begriff-Vorschlag:
  //--------------------------------------------------------------
  function onSendIdeaProposalClicked(problemReport) {
    storeTerm = document.querySelector("#userBegriffVorschlag").value;
      
    if(storeTerm !== "") {
      problemReport.innerHTML = "";
      //speichere in Firebase-Database:
      begriffeRef.once("value").then(function(snapshot) {
        if(snapshot.hasChild(storeTerm) === false) {
          begriffeRef.child(storeTerm).set({
            checkedByAdmin: false,
          });
        }
        document.querySelector("#userBegriffVorschlag").value = "";
        //schließe Popup:
        closePopUp("#vorschlagPopup");
      });
    } else {
      problemReport.innerHTML = "Bitte gib deinen Begriffvorschlag ein, bevor du ihn abschickst!";
    }
  }
  /*
  *=============================================================================================== 
  * PLAY!!!!
  *===============================================================================================
  */
  function initPlayListener() {
    montagsMalerController.registerOnPinHoverListener(play);
    montagsMalerController.registerPlayListener(play); 
  }
    
  function onPlayButtonClick() {
    montagsMalerModel.deactivatePrevSeite();
    montagsMalerModel.activateSeite("#spielseite");
    gamemanager.init();
  }
    
  function exitPlayScene() {
    montagsMalerModel.deactivatePrevSeite();
    location.reload();
  }
    
  //bekomme zufälligen Begriff für das Spiel:
  //=============================================================
  function retrieveBegriffFromFirebase(begriffPanel) {  
    begriffeRef.once("value").then(function(snapshot) {
      numTerms = snapshot.numChildren();
      randomTermIndex = getRandomIndex(numTerms);
        
      termObject = snapshot.val();
      termArray = Object.getOwnPropertyNames(termObject);
      randomTerm = termArray[randomTermIndex];
        
      countTermOccurrence = 0;
      for (i = 0; i < tempArray.length; i++) {
        if (tempArray[i] === randomTerm) {
          countTermOccurrence = countTermOccurrence + 1;
        }
      }
      //------------------------------------
      // Begriff war davor nicht benutzt:
      //------------------------------------
      if (countTermOccurrence === 0) {
        tempArray.push(randomTerm);
        
        if (snapshot.child(randomTerm).hasChild("checkedByAdmin") === true) {
          if (snapshot.child(randomTerm + "/checkedByAdmin").val() === true) {
            //checkedByAdmin = true: Begriff kann geladen und in den Spielmodus hinzugefügt werden
            document.querySelector(begriffPanel).innerHTML = randomTerm;
            sessionStorage.currentQuiz = randomTerm;
            gamemanager.forwardQuizToSocket(randomTerm);  
          } else {
            //checkedByAdmin = false: Begriff kann nicht geladen und hizugefügt werden && starte von vorne
            retrieveBegriffFromFirebase(begriffPanel);
          }
        } else {
          document.querySelector(begriffPanel).innerHTML = randomTerm;
          sessionStorage.currentQuiz = randomTerm;
          gamemanager.forwardQuizToSocket(randomTerm);    
        }  
      //------------------------------------
      // Begriff wurde zuvor benutzt:
      //------------------------------------ 
      } else if (countTermOccurrence === 1 && tempArray.length !== numTerms) {
        retrieveBegriffFromFirebase(begriffPanel);
      //------------------------------------
      // alle Begriffe wurden zuvor benutzt:
      //------------------------------------
      } else if (countTermOccurrence === 1 && tempArray.length === numTerms) {
        tempArray = [];
        retrieveBegriffFromFirebase(begriffPanel);
      }    
    });
  }
    
  function getRandomIndex(numTerm) {
    return Math.floor((Math.random() * numTerm - 1) + 1);
  }
    
  //addiere Gewinn-Punkte:
  //=============================================================
  function storeWinPointsInFirebase(user) {
    //lade Nutzer-Punktestand:
    userRef.once("value").then(function(snapshot) {
      userScore = snapshot.child(user+ "/score").val();
      userScore = userScore + 5;
        
      //speichere neuen Nutzer-Punktestand:
      userRef.child(user).update({
        score: userScore,
      });
        
    });
  }
  /*
  *=============================================================================================== 
  * persönlicher Punktestand:
  *===============================================================================================
  */
  function initOwnScoreListener() {
    getOwnPlaceAndScoreFormFirebase(localStorage.loggedUser);
  }
    
  function getOwnPlaceAndScoreFormFirebase(user) {
    //lade aus Localstorage
    document.querySelector("#own_place").innerHTML = localStorage.ownPlace;
    document.querySelector("#own_scoring").innerHTML = localStorage.ownScore;
      
    //lade aus Firebase-Database
    userRef.once("value").then(function(snapshot) {
      ownScoring = snapshot.child(user+ "/score").val();
      //speichere in LocalStorage
      localStorage.ownScore = ownScoring;
      document.querySelector("#own_scoring").innerHTML = ownScoring;
      setOwnPlace(user);
    });
  }
    
  function setOwnPlace(user) {
    userRef.once("value").then(function(snapshot) {
      counter = 0;
      numUser = snapshot.numChildren();
      userRef.orderByChild("score").on("child_added", function(childSnapshot) {
        counter = counter + 1;
        if (childSnapshot.key === user) {
          //speichere in LocalStorage:
          localStorage.ownPlace = numUser - (counter-1);
          //lade aus Firebase:
          document.querySelector("#own_place").innerHTML = numUser - (counter-1);
        }
      });
    });
  } 
  
  that.onSendIdeaProposalClicked = onSendIdeaProposalClicked;
  that.onChangePwButtonClicked = onChangePwButtonClicked;
  that.onSignupButtonClicked = onSignupButtonClicked;
  that.onLoginPlayButtonClicked = onLoginPlayButtonClicked;
  that.onLoginButtonClicked = onLoginButtonClicked;
  that.openPopUp = openPopUp;
  that.closePopUp = closePopUp;
  that.onSendIdeaProposalClicked = onSendIdeaProposalClicked;
  that.onSignInRetrieveDataFromFirebase = onSignInRetrieveDataFromFirebase;
  that.onChangePWReadAndWriteDateFromFirebase = onChangePWReadAndWriteDataFromFirebase;
  that.onSignUpCreateDataInFirebase = onSignUpCreateDataInFirebase;
  that.onPinnwandButtonClick = onPinnwandButtonClick;
  that.onPlayButtonClick = onPlayButtonClick;
  that.retrieveBegriffFromFirebase = retrieveBegriffFromFirebase;
  that.storeWinPointsInFirebase = storeWinPointsInFirebase;
  that.exitPlayScene = exitPlayScene;
  that.init = init;
  return that;
}());
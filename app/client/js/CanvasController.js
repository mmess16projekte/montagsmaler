var Painter = Painter || {};
Painter.CanvasController = function (socketController){
  "use strict";
    /* eslint-env browser  */

  var that = {},
    canvas,
    context,
    options,
    img,
    savedStates = [],
    mouse = {
      lastX: -1,
      lastY: -1,
      clicked: false,
    };

    //================================================================
    //Alle Funktionen im Canvasbereich:
    //================================================================
    
    //Zeichnen auf Canvas bei Observern
  function drawReceivedLine(data){
    var line = data.line;
    context.beginPath();
    context.moveTo(line[0].x, line[0].y);
    context.lineTo(line[1].x, line[1].y);
    context.strokeStyle = line[2];
    context.lineWidth = options.weight;
    context.stroke();
  }
    
    //Canvas leeren
  function clearCanvas(){
    context.clearRect(0, 0, canvas.width, canvas.height); 
  }

  function saveState() {
    savedStates.push(getDataUrl());
  }

  function restoreState(state) {
    var image;
    if (state === undefined) {
      return;
    }
    image = new Image();
    image.addEventListener("load", function () {
      clear();
      drawImage(image, 0, 0);
    });
    image.src = state;
  }

  function drawImage(img, x, y) {
    context.drawImage(img, x, y);
  }
    
    //je nach Bildschirmgröße wird canvas angepasst
  function resizeCanvas(){
    context.width = $("#canvas").width();
    context.height = $("#canvas").height();
    canvas.width = $("#canvas").width();
    canvas.height = $("#canvas").height();
  }

  //Zeichnen auf Canvas wenn Painter
  function drawLine(start, end, isObserver) {
    if (start.x === -1 || start.y === -1) {
      return;
    }
    context.beginPath();
    context.moveTo(start.x, start.y);
    context.lineTo(end.x, end.y);
    context.strokeStyle = options.color;
    context.lineWidth = options.weight;
    context.stroke();
    context.closePath();
    if (isObserver === false){
      socketController.emitLinePainter(start.x, start.y, end.x, end.y, options.color);
    }
  }
 
  function drawLineFromSocketMessage(lineData){
    var start = lineData.data.line[0],
      end = lineData.data.line[1],
      color = lineData.data.line[2];
    options.color = color;
    drawLine(start, end, true);
  }
   
    //================================
    //Alle Funktionen im Canvasbereich:
    //================================
  function updateMouseInformation(x, y, clicked) {
    mouse.lastX = x;
    mouse.lastY = y;
    mouse.clicked = clicked; 
  }

  function onMouseMovedInCanvas(event) {
    if (!mouse.clicked) {
      return;
    }
    switch (options.tool) {
      case "line":
        drawLine({
          x: mouse.lastX,
          y: mouse.lastY,
        }, {
          x: event.offsetX,
          y: event.offsetY,
        },
          false);
        break;
      default:
        break;
    }
    updateMouseInformation(event.offsetX, event.offsetY, mouse.clicked);
  }

  function onMouseLeftCanvas() {
    updateMouseInformation(-1, -1, false);
  }

  function onMouseDownInCanvas(event) {
    preventObserverPainting();
    saveState();
    updateMouseInformation(event.offsetX, event.offsetY, true);
    switch (options.tool) {
      default:
        break;
    }
  }

    //verhindern dass observer auch malen können
  function preventObserverPainting(){
    if(sessionStorage.userRole === "observer"){
      canvas.removeEventListener("mousemove", onMouseMovedInCanvas);
      canvas.removeEventListener("mouseleave", onMouseLeftCanvas);
      canvas.removeEventListener("mousedown", onMouseDownInCanvas);
      canvas.removeEventListener("mouseup", onMouseUpInCanvas);
    }
      
  }
    
    //verhindern dass nach Spielende noch gemalt werden kann
  function endOfGamePreventPainting(){
    canvas.removeEventListener("mousemove", onMouseMovedInCanvas);
    canvas.removeEventListener("mouseleave", onMouseLeftCanvas);
    canvas.removeEventListener("mousedown", onMouseDownInCanvas);
    canvas.removeEventListener("mouseup", onMouseUpInCanvas);
  }
    
  function onMouseUpInCanvas(event) {
    updateMouseInformation(event.offsetX, event.offsetY, false);
  }

  function clear(clearStates) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    if (clearStates) {
      savedStates = [];
    }
    socketController.emitClearCanvas();
  }

    //DataURl für Speichern in Blob
  function getDataUrl() {
    return canvas.toDataURL();
  }
 
  function setOptions(newOptions) {
    options = newOptions;
  }

  function init(canvasNode) {
    canvas = canvasNode;
    context = canvas.getContext("2d");
    canvas.addEventListener("mousemove", onMouseMovedInCanvas);
    canvas.addEventListener("mouseleave", onMouseLeftCanvas);
    canvas.addEventListener("mousedown", onMouseDownInCanvas);
    canvas.addEventListener("mouseup", onMouseUpInCanvas);
    resizeCanvas();
  }

  that.init = init;
  that.setOptions = setOptions;
  that.getImage = getDataUrl;
  that.clear = clear;
  that.clearCanvas = clearCanvas;
  that.drawReceivedLine = drawReceivedLine;
  that.resizeCanvas = resizeCanvas;
  that.endOfGamePreventPainting = endOfGamePreventPainting;
  that.drawLineFromSocketMessage = drawLineFromSocketMessage;
  return that;
};

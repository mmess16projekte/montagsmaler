var GameManager = GameManager || {};
GameManager.SocketController = function () {
  "use strict";

  var that = new EventPublisher(), 
    socket, listenElement, user, rootMessageBox, rateBegriff, i, inputBegriff, inputUser, usercolour, can, con;
    
  usercolour = ["red", "green", "blue", "blueviolet", "indigo", "maroon", "mediumorchid", "lightseagreen", "indianred",];

    
    
  function init() {
    socket = io.connect();
    initSocketEvents();
    socket.emit("ask_for_role");
  }
    
    //-------------------------------------------------------------
    //Alle Socket Verbindungen mit Server:
    //-------------------------------------------------------------
  function initSocketEvents(){
      
      //Bei Aufruf der Rolle
    socket.on("role_message", function(roleData){
      that.notifyAll("onRoleReceived", roleData);    
    });

     //Bei Zeichnen auf Canvas
    socket.on("draw_line", function (data) { 
      that.notifyAll("onLineReceived", data);
    });

      //Bei Klick auf Canvas leeren
    socket.on("clear_canvas", function(){  
      can = document.getElementById("canvas");
      con = canvas.getContext("2d");       
      con.clearRect(0,0,can.width, can.height);
    });
        
      //Messages im Chat mit farbigem Username
    $("form").submit(function(){
      var colouredUserName = "<font color='"+localStorage.userColour+"'>"+localStorage.loggedUser+"</font>";
      socket.emit("chat message", colouredUserName +": " + $("#m").val());
        
      rateBegriff = sessionStorage.currentQuiz;
        
      inputBegriff = $("#m").val();
        
      inputUser = localStorage.loggedUser;
        
        //Ratebegriff mit Chatinput vergleichen:
      checkIfInputIsGesuchterBegriff(inputUser, inputBegriff, rateBegriff); 
        
      $("#m").val("");
      return false;
    });
      
      //Bei Klick auf Neue Runde durch Painter, Info an Chat
    socket.on("newRound_informChat", function(){
      var newRoundMsg = "<b>Eine neue Runde wurde gestartet!</b>";
      listenElement = document.createElement("li");
      listenElement.innerHTML=newRoundMsg;
      rootMessageBox = document.querySelector("#messages");
      rootMessageBox.appendChild(listenElement);
    });
      
      //Bei Spielende, Info an Chat mit Gewinnername
    socket.on("inform_chat", function(winner){
      var statusmsg = "Die Runde ist zu Ende. User <b>"+winner+"</b> hat gewonnen!";
      socket.emit("restart_app");
      listenElement = document.createElement("li");
      listenElement.innerHTML=statusmsg;
      rootMessageBox = document.querySelector("#messages");
      rootMessageBox.appendChild(listenElement);
      if(sessionStorage.userRole === "observer"){
        window.setTimeout(function(){MontagsMalerApp.exitPlayScene();}, 4000);
      }
      else{
        that.notifyAll("endOfGame");
      }
    });
      
      //Nachrichten im Chat übertragen
    socket.on("chat message", function(msg){
      if (msg !== "") {
        listenElement = document.createElement("li");
        listenElement.innerHTML = msg;
        rootMessageBox = document.querySelector("#messages");
        rootMessageBox.appendChild(listenElement);
      }
    });
  }
    
    //-------------------------------------------------------------
    //Alle Emit-Nachrichten an den Server durch SocketIO:
    //-------------------------------------------------------------
    
    //Auf Canvas zeichnen 
  function emitLinePainter(startx, starty, endx, endy, color){
    socket.emit("draw_line", { line: [ {x: startx, y: starty,}, {x: endx, y: endy,}, color, ], });
  }
    //Das Canvas leeren
  function emitClearCanvas(){
    socket.emit("clear_canvas");
  }
    //Neue Runde starten
  function emitNewRound(){
    socket.emit("newRound_informChat");
  }
    //Ratebegriff weiterleiten
  function emitQuizToServer(quiz){
    socket.emit("retrieve_quiz", quiz); 
  } 
    //Benutzername weiterleiten
  function emitUserToServer(user){
    socket.emit("inform_chat", user);
  }
    //Ratebegriff von Server abfragen
  function initQuizRetrieval(){
    socket.on("retrieve_quiz", function(quiz){
      sessionStorage.currentQuiz = quiz;
      if(sessionStorage.userRole === "observer"){
        $(".ratebegriff").text(quiz);
      }
    });        
  }
   
    //Ratebegriff mit Chatinput überprüfen durch den GameManager
  function checkIfInputIsGesuchterBegriff(user, input, begriff) {
    that.notifyAll("checkRatebegriff", {user, input, begriff,});
  }

  that.init = init;
  that.emitUserToServer = emitUserToServer;
  that.emitNewRound = emitNewRound;
  that.emitLinePainter = emitLinePainter;
  that.emitClearCanvas = emitClearCanvas;
  that.emitQuizToServer = emitQuizToServer;
  that.initQuizRetrieval = initQuizRetrieval;
  
  return that;
};

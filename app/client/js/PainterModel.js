var Painter = Painter || {};
Painter.PainterModel = function () {
    "use strict";
    /* eslint-env browser  */
    /* global EventPublisher */

    var that = new EventPublisher(),
        LINE_WEIGHT = 2,
        color,
        tool;

    function setColor(newColor) {
        color = newColor;
        that.notifyAll("optionsChanged", getState());
    }

    function setTool(newTool) {
        tool = newTool;
        that.notifyAll("optionsChanged", getState());
    }

    function getState() {
        return {
            color: color,
            tool: tool,
            weight: LINE_WEIGHT
        };
    }

    that.setColor = setColor;
    that.setTool = setTool;
    that.getState = getState;
    return that;
};

var GameManager = GameManager || {};
GameManager.Painter = function(socketController, isObserver) {
  "use strict";
    /* eslint-env browser  */
  var that = new EventPublisher(),
    model,
    storageRef = firebase.storage().ref(),
    bilderRef = firebase.database().ref("Bilder"),      
    metadata = {
      contentType: "image/jpeg",
    },      
    picture, pictureToBlob, file, canvas, uploadTask,
    byteString, mimeString, ab, ia, i, bb,      
    username, now, usernameDate,      
    term, downloadURL,        
    problemReport,      
    toolbox,
    palette;
    
/*
  *=============================================================================================== 
  * Bild in Firebase Datenbank einspeisen:
  *===============================================================================================
*/
    
  function canvasToBlob() {
    //hole aktuellen Benutzernamen:
    username = localStorage.loggedUser;
    //hole aktuelles Bild auf dem Canvas als dataURI:
    picture = canvas.getImage();
    //hole aktuelles Datum:
    now = new Date();
    now.getTime();
    //Kombiniere Benutzernamen mit Datum für individuellen Key und dessen Speicherung in der Datenbank und Storage
    usernameDate = username + " " + now;
    //Übertrage dataURI in Blob-Format:
    pictureToBlob = dataURItoBlob(picture);
    file = pictureToBlob;
    
    //================================
    //Lade Blob in Firebase Storage hoch:
    //================================
    //https://firebase.google.com/docs/storage/web/upload-files
    uploadTask = storageRef.child("images/" + usernameDate).put(file, metadata);
    //Beachte Statusänderungen, Errors und Abschließen des Uploads.
    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // oder 'state_changed'
  function(snapshot) {
    //Hole Task Fortschritt inklusive Anzahl an hochgeladenen bytes und insgesamte Anzahl an noch zu ladenden bytes
    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    console.log("Upload is " + progress + "% done");
    switch (snapshot.state) {
      case firebase.storage.TaskState.PAUSED: // or 'paused'
        console.log("Upload is paused");
        break;
      case firebase.storage.TaskState.RUNNING: // or 'running'
        console.log("Upload is running");
        break;
    }
    //Nach Abschluss des Uploads:
    if(progress === 100) {
    //Hole downloadUrl aus dem Firebase Storage und speichere sie in LocalStorage für Upload in der Firebase Datenbank
      downloadURL = uploadTask.snapshot.downloadURL;
      localStorage.url = downloadURL;
      setTimeout(function(){saveDownloadURLInDatabase(usernameDate);}, 5000);
    }           
  }); 
  }
    
    //=======================================
    //Speichere downloadUrl in Firebase-Datenbank:
    //=======================================
  function saveDownloadURLInDatabase(name) {
    //hole Bezeichnung von Html-DOM-Element
    term = document.querySelector(".ratebegriff").innerHTML;
    //speichere downloadUrl in Firebase-Datenbank
    bilderRef.once("value").then(function(snapshot) {
      if(snapshot.hasChild(name) === false) {
        bilderRef.child(name).set({
          Begriff: term,
          url: localStorage.url,
        });
      } else {
        //Bei Problemen:
        problemReport = document.querySelector("#gameEndPopUp").children[0].children[1];
        problemReport.innerHTML = "";
        problemReport.innerHTML = "ERROR! Das Bild konnte nicht gepostet werden!";
      }
    });
    //exitGame in GameManager nachdem upload erfolgreich war oder Fehler stattfindet
    that.notifyAll("onURLinDatabase");
  }
   
  //====================
  //Umwandlung von URI in Blob:
  //====================
//Quelle: https://gist.github.com/fupslot/5015897
  function dataURItoBlob(dataURI) {
    // wandle base64 in raw binary data um aus einem string
    byteString = atob(dataURI.split(",")[1]);
    // extrahiere mime komponente
    mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    // schreibe string bytes in ArrayBuffer
    ab = new ArrayBuffer(byteString.length);
    ia = new Uint8Array(ab);
    for (i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    // schreibe ArrayBuffer in einen blob
    bb = new Blob([ab,]);
    return bb;
  }
    
/*
  *=============================================================================================== 
  * Code für Painter  
  *===============================================================================================
*/

  function onToolSelected(event) {
    model.setTool(event.data);
  }

  function onColorSelected(event) {
    model.setColor(event.data);
  }

  function onActionSelected(event) {
    switch (event.data) {
      case "new":
        console.log("CASE NEW");
        canvas.clear();
        break;
      default:
        break;
    }
  }

  function onOptionsChanged(event) {
    canvas.setOptions(event.data);
  }

  function init() {
    initModel();
    initCanvas();
    initPalette();
    initToolbox();
  }

  //====================================================
  //Alle Initialisierungen der Komponenten über Painter
  //====================================================
    
  function initModel() {
    model = new Painter.PainterModel();
    model.addEventListener("optionsChanged", onOptionsChanged);
  }

  function initCanvas() {
    canvas = new Painter.CanvasController(socketController);
    canvas.init(document.querySelector(".canvas"));
  }

  function initPalette() {
    palette = new Painter.PaletteView();
    palette.init(document.querySelector(".colors"));
    palette.addEventListener("colorSelected", onColorSelected);
    palette.setDefault();
  }

  function initToolbox() {
    toolbox = new Painter.ToolboxView();
    toolbox.init(document.querySelector(".tools"));
    toolbox.addEventListener("toolSelected", onToolSelected);
    toolbox.addEventListener("actionSelected", onActionSelected);
    toolbox.setDefault();
  }    
    
  function callCanvasSize(){
    canvas.resizeCanvas();
  }
    
    //zeichne Linienübertragung von Socket auf Canvas
  function drawLine(lineData){
    canvas.drawLineFromSocketMessage(lineData);
  }
    
    //nach Spielende darf nicht mehr gezeichnet werden
  function preventCanvasDrawing(){
    canvas.endOfGamePreventPainting();
  }   

  that.init = init;
  that.drawLine = drawLine;
  that.canvasToBlob = canvasToBlob;
  that.callCanvasSize = callCanvasSize;
  that.preventCanvasDrawing = preventCanvasDrawing;
  return that;
};

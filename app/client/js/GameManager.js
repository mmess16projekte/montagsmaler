var MontagsMalerApp = MontagsMalerApp || {};
MontagsMalerApp.GameManager = function() {
  "use strict";
    /* eslint-env browser  */

  var that = {},
    painter, socketController,
    picPostButton,
    exitGamebutton,
    montagsMalerController,
    canvas,
    uploadTask,
    username,
    usercolour, user, input, begriff, oldquizz, problemReport, loadingBanana;
   
  usercolour = ["red", "green", "blue", "blueviolet", "indigo", "maroon", "mediumorchid", "lightseagreen", "indianred", "hotpink", "darkcyan", "coral",];
    
  function init(){
    initSocketController();
    initPainter();
    initPicPostButton();
    initExitGameButton();
    initNewRoundButton();
    initUserColour();
      
    socketController.initQuizRetrieval();
    painter.callCanvasSize();
  }
    
    //-------------------------------------------------------------
    //Initialisieren aller Komponenten über den GameManager:
    //-------------------------------------------------------------
  function initSocketController(){
    socketController = new GameManager.SocketController();
    socketController.addEventListener("onLineReceived", onLineReceived);
    socketController.addEventListener("onRoleReceived", onRoleReceived);
    socketController.addEventListener("endOfGame", openPublishPicturePopUp);
    socketController.addEventListener("checkRatebegriff", checkRateBegriff);

    socketController.init();
  }
    
  function initPainter(){
    painter = new GameManager.Painter(socketController);
    painter.addEventListener("onURLinDatabase", exitGame);
    painter.init();
  }
    
  function initPicPostButton() {
    picPostButton = document.getElementById("jaButton");
    picPostButton.addEventListener("click", postPic);
  }
    
  function initExitGameButton(){
    exitGamebutton = document.getElementById("neinButton");
    exitGamebutton.addEventListener("click", exitGame);
  }
    
  function initNewRoundButton() {
    picPostButton = document.getElementById("neueRundeButton");
    picPostButton.addEventListener("click", neueRunde);
  }
    
  function initUserColour(){
    localStorage.userColour = usercolour[Math.floor(Math.random() * usercolour.length)];
  }
      
    //Beendet das Spiel und leitet Spieler in die Startseite zurück
  function exitGame(){
    window.setTimeout(function(){MontagsMalerApp.exitPlayScene();}, 1000);
  }
    
    //Überprüfung von Ratebegriff un Chatinput mit den Daten aus Socket
  function checkRateBegriff(data){
    user = data.data.user;
    input = data.data.input;
    begriff = data.data.begriff;

    if (input.toLowerCase().trim().replace(/[^a-zA-ZÄÖÜßäöü]+/g, "") === begriff.toLowerCase().trim()) {
      socketController.emitUserToServer(user);
      MontagsMalerApp.storeWinPointsInFirebase(user);
    }
  }
    
    //Bei Start einer neuen Runde wird neuer Ratebegriff abgefragt und das Canvas geleert
  function neueRunde(){
    socketController.emitClearCanvas();
    if(sessionStorage.userRole === "painter"){
      oldquizz = $(".ratebegriff").text();
      MontagsMalerApp.retrieveBegriffFromFirebase(".ratebegriff"); 
      socketController.emitNewRound();
    }      
  }
    
    //Ratebegriff übertragen
  function forwardQuizToSocket(quizz){  
    socketController.emitQuizToServer(quizz);
  }

    //Bild wird gepostet nachdem Painter sich dafür am Spielende entscheidet
  function postPic() {
    problemReport = document.querySelector("#gameEndPopUp").children[0].children[1];
    problemReport.innerHTML = "Ihr Kunstwerk wird gerade auf der Pinnwand gepostet. Dies kann ein paar Sekunden dauern. Bitte warten Sie einen Augenblick, Sie werden automatisch auf die Startseite zurückgeleitet.";
    picPostButton = document.getElementById("jaButton");
    picPostButton.className = "statusHidden";
    exitGamebutton.className = "statusHidden";
    picPostButton.parentElement.className = "statusHidden";
    loadingBanana = document.getElementById("loadingBanana");
    loadingBanana.className = "status";
    painter.canvasToBlob();
  }
 
    //Linie im Canvas wird übertragen
  function drawLine(lineData){
    painter.drawLine(lineData);
  }
    
    //Nur wenn Rolle Observer ist, wird Linie übertragen, sonst doppelt.
  function onLineReceived(line){
    drawLine(line);
  }
    
    //Rolle wird abgefragt und die entsprechenden Interaktionselemente
    //eingeblendet (Entweder Chat, oder Malwerkzeuge mit Ratebegriff)
  function onRoleReceived(roleData){
    if (roleData.data["role"] === "observer"){
      sessionStorage.userRole = "observer";
        
      document.getElementById("m").style.visibility="visible";
      document.getElementById("sendBtn").style.visibility="visible";
    }
    else{
      sessionStorage.userRole = "painter";
      
      MontagsMalerApp.retrieveBegriffFromFirebase(".ratebegriff");
     
      document.getElementsByClassName("sidebar")[0].style.visibility="visible";
      document.getElementById("neueRundeButton").style.visibility="visible";
      document.getElementsByClassName("ratebegriff")[0].style.visibility="visible"; 
    }
  }
    
    //Pop-Up bei Spielende für Painter
  function openPublishPicturePopUp(){
    document.querySelector("#gameEndPopUp").classList.remove("hidden");
    document.querySelector("#gameEndPopUp").classList.add("fadeIn");
    document.querySelector(".testTemplateBinding").classList.add("lowerOpacity");
    
    painter.preventCanvasDrawing();
  }
    
  that.init = init;
  that.forwardQuizToSocket = forwardQuizToSocket;
  that.checkRateBegriff = checkRateBegriff;
  that.exitGame = exitGame;
  return that;
};
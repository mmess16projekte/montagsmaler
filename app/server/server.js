var express = require("express"), 
  app = express(),
  http = require("http"),
  socketIo = require("socket.io"),
  line_history = [], 
  painterAssigned = false, 
  painterSocketNumber = 0,
  currentquiz,
  winner,
  allClients = [],
  server = http.createServer(app),
  io = socketIo.listen(server);

//====================================================
//Setze Verbindung zum Server auf und initailisiere URL
//====================================================

server.listen(8080);
app.use(express.static("../client"));
console.log("Server running on 132.199.139.24:8080");

//====================================================
//Alle Handler für hinzukommende Socket Verbindungen
//====================================================

io.on("connection", function (socket) {
    
  allClients.push(socket);

  //Handler für Tab schließen, Internetabbruch etc.
  //Informiert Chat wenn Painter die Verbindung abbricht
  socket.on("disconnect", function() {
    var i = allClients.indexOf(socket);
    allClients.splice(i, 1);
       
    if(i < painterSocketNumber){
      painterSocketNumber = painterSocketNumber-1;
    }
       
    if(painterSocketNumber === i){
      line_history = []; 
      painterAssigned = false;
      io.emit("clear_canvas");
      io.emit("chat message", "<b>Der Künstler hat sich verarbschiedet. Nächster Künstler gibt neuen Ratebegriff vor.</b>");
    }
          
    // Wenn keine Nutzer mehr mit Socket verbunden sind werden Datenstrukturen zurückgesetzt
    // da sonst der Server keinen Painter mehr zulässt   
    if(allClients.length === 0){
      line_history = []; 
      painterAssigned = false;
    }  
  });
  
  //Übertragung der bereits gemalten Linien in die line_history
  for (var i in line_history) {
    socket.emit("draw_line", { line: line_history[i], } );
  }
    
  //Übermittlung des Ratebegriffs für sessionStorage
  io.emit("retrieve_quiz", currentquiz);

  //Handler für Zeichnungen auf dem Canvas
  socket.on("draw_line", function (data) {
    line_history.push(data.line);
    io.emit("draw_line", { line: data.line, });
  });

  //Handler für Canvas leeren    
  socket.on("clear_canvas", function (data) {
    line_history = [];  
    io.emit("clear_canvas");
  });
    
  //Handler für Nachrichten im Chat
  socket.on("chat message", function(msg){
    io.emit("chat message", msg);
  });
    
  //Vermittlung der Rollen bei neuen Mitspielern
  socket.on("ask_for_role", function(){        
    if (painterAssigned === false){
      painterAssigned = true;
      painterSocketNumber = allClients.indexOf(socket);
      socket.emit ("role_message", {role: "painter",});
    }
    else{
      socket.emit("role_message", {role: "observer",});
    }  
  });
    
  //Aktualisieren des Ratebegriffs
  socket.on("retrieve_quiz", function(quiz){
    currentquiz = quiz;
    io.emit("retrieve_quiz", currentquiz);
  });
    
  //Informieren des Chats bei Spielende mit Gewinnername
  socket.on("inform_chat", function(user){
    winner = user;
    io.emit("inform_chat", winner);
  });
    
  //Informieren des Chats bei Starten einer neuen Runde durch Painter
  socket.on("newRound_informChat", function(){
    io.emit("newRound_informChat");
  });
    
  //Zurückstellen aller Datenstrukturen bei Spielende
  socket.on("restart_app", function(){
    line_history = [];
    //painterAssigned = false;
  });
});